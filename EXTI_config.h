/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          March 20th, 2019                    */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#ifndef EXTI_CONFIG_H_
#define EXTI_CONFIG_H_



#define EXTI_INT0_STATE						EXTI_INTERRUPT_ENABLED
#define EXTI_INT1_STATE						EXTI_INTERRUPT_DISABLED
#define EXTI_INT2_STATE						EXTI_INTERRUPT_DISABLED


#define EXTI_INT0_SENSE_LEVEL				EXTI_SENSE_LEVEL_FALLING_EDGE
#define EXTI_INT1_SENSE_LEVEL				EXTI_SENSE_LEVEL_FALLING_EDGE
#define EXTI_INT2_SENSE_LEVEL				EXTI_SENSE_LEVEL_FALLING_EDGE


#define EXTI_INT0_CALLBACK_FUNCTION			NULL
#define EXTI_INT1_CALLBACK_FUNCTION			NULL
#define EXTI_INT2_CALLBACK_FUNCTION			NULL





#endif /* EXTI_CONFIG_H_ */
