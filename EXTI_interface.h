/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          March 20th, 2019                    */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#ifndef EXTI_INTERFACE_H_
#define EXTI_INTERFACE_H_


#define EXTI_u8_INT0							(u8)6
#define EXTI_u8_INT1							(u8)7
#define EXTI_u8_INT2							(u8)5

#define EXTI_INTERRUPT_ENABLED					(u8)10
#define EXTI_INTERRUPT_DISABLED					(u8)20

#define EXTI_SENSE_LEVEL_LOW_LEVEL				0b00
#define EXTI_SENSE_LEVEL_ON_CHANGE				0b01
#define EXTI_SENSE_LEVEL_FALLING_EDGE			0b10
#define EXTI_SENSE_LEVEL_RISING_EDGE			0b11






void EXTI_voidInit(void);
u8 EXTI_u8EnableInterrupt(u8 Copy_u8InterruptNum);
u8 EXTI_u8DisableInterrupt(u8 Copy_u8InterruptNum);
u8 EXTI_u8SetSenseLevel(u8 Copy_u8InterruptNum, u8 Copy_u8InterruptSenseLevel);
u8 EXTI_u8SetCallback(u8 Copy_u8InterruptNum, void (*Copy_Pu8CallbackFunction) (void));



#endif /* EXTI_INTERFACE_H_ */
