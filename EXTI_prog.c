/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          March 20th, 2019                    */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */


#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "DELAY.h"

#include "EXTI_interface.h"
#include "EXTI_private.h"
#include "EXTI_config.h"


static void (* EXTI_PAVoidCallback[EXTI_NO_OF_INTERRUPTS]) (void);


void __vector_1(void)
{
	if(EXTI_PAVoidCallback[0] != NULL)
	{
		EXTI_PAVoidCallback[0]();
		SET_BIT(EXTI_GIFR,EXTI_u8_INT0);
	}
}

void __vector_2(void)
{
	if(EXTI_PAVoidCallback[1] != NULL)
	{
		EXTI_PAVoidCallback[1]();
		SET_BIT(EXTI_GIFR,EXTI_u8_INT1);
	}
}

void __vector_3(void)
{
	if(EXTI_PAVoidCallback[2] != NULL)
	{
		EXTI_PAVoidCallback[2]();
		SET_BIT(EXTI_GIFR,EXTI_u8_INT2);
	}
}


void EXTI_voidInit(void)
{


	// Initializing the state
	ASG_BIT(EXTI_GICR, EXTI_u8_INT0, EXTI_INT0_STATE);
	ASG_BIT(EXTI_GICR, EXTI_u8_INT1, EXTI_INT1_STATE);
	ASG_BIT(EXTI_GICR, EXTI_u8_INT2, EXTI_INT2_STATE);


	// Initializing the sense levels
	CLR_BIT(EXTI_MCUCR,0);
	CLR_BIT(EXTI_MCUCR,1);
	EXTI_MCUCR |= (EXTI_INT0_SENSE_LEVEL << 0);

	CLR_BIT(EXTI_MCUCR,2);
	CLR_BIT(EXTI_MCUCR,3);
	EXTI_MCUCR |= (EXTI_INT1_SENSE_LEVEL << 2);

	if(EXTI_INT2_SENSE_LEVEL == EXTI_SENSE_LEVEL_FALLING_EDGE)
	{
		CLR_BIT(EXTI_MCUCSR, 6);
		EXTI_MCUCSR |= (EXTI_INT2_SENSE_LEVEL_FALLING_EDGE << 6);
	}
	else if(EXTI_INT2_SENSE_LEVEL == EXTI_SENSE_LEVEL_RISING_EDGE)
	{
		CLR_BIT(EXTI_MCUCSR, 6);
		EXTI_MCUCSR |= (EXTI_INT2_SENSE_LEVEL_RISING_EDGE << 6);
	}



	// Initializing the callback functions
	EXTI_PAVoidCallback[0] = EXTI_INT0_CALLBACK_FUNCTION;
	EXTI_PAVoidCallback[1] = EXTI_INT1_CALLBACK_FUNCTION;
	EXTI_PAVoidCallback[2] = EXTI_INT2_CALLBACK_FUNCTION;



	return;
}



u8 EXTI_u8EnableInterrupt(u8 Copy_u8InterruptNum)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if( (Copy_u8InterruptNum == EXTI_u8_INT0) ||
		(Copy_u8InterruptNum == EXTI_u8_INT1) ||
		(Copy_u8InterruptNum == EXTI_u8_INT2))
	{
		SET_BIT(EXTI_GICR,Copy_u8InterruptNum);

		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}



u8 EXTI_u8DisableInterrupt(u8 Copy_u8InterruptNum)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if( (Copy_u8InterruptNum == EXTI_u8_INT0) ||
		(Copy_u8InterruptNum == EXTI_u8_INT1) ||
		(Copy_u8InterruptNum == EXTI_u8_INT2))
	{
		CLR_BIT(EXTI_GICR,Copy_u8InterruptNum);

		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}


u8 EXTI_u8SetSenseLevel(u8 Copy_u8InterruptNum, u8 Copy_u8InterruptSenseLevel)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if( ((Copy_u8InterruptNum == EXTI_u8_INT0) ||
		 (Copy_u8InterruptNum == EXTI_u8_INT1) ||
		 (Copy_u8InterruptNum == EXTI_u8_INT2))

		&&

		((Copy_u8InterruptSenseLevel == EXTI_SENSE_LEVEL_LOW_LEVEL) ||
		 (Copy_u8InterruptSenseLevel == EXTI_SENSE_LEVEL_ON_CHANGE) ||
		 (Copy_u8InterruptSenseLevel == EXTI_SENSE_LEVEL_FALLING_EDGE) ||
		 (Copy_u8InterruptSenseLevel == EXTI_SENSE_LEVEL_RISING_EDGE)))

	{
		switch (Copy_u8InterruptNum)
		{
			case EXTI_u8_INT0:
				CLR_BIT(EXTI_MCUCR,0);
				CLR_BIT(EXTI_MCUCR,1);
				EXTI_MCUCR |= (Copy_u8InterruptSenseLevel << 0);
				Local_u8Error = STD_u8_OK;
				break;

			case EXTI_u8_INT1:
				CLR_BIT(EXTI_MCUCR,2);
				CLR_BIT(EXTI_MCUCR,3);
				EXTI_MCUCR |= (Copy_u8InterruptSenseLevel << 2);
				Local_u8Error = STD_u8_OK;
				break;

			case EXTI_u8_INT2:
				if(Copy_u8InterruptSenseLevel == EXTI_SENSE_LEVEL_FALLING_EDGE)
				{
					CLR_BIT(EXTI_MCUCSR, 6);
					EXTI_MCUCSR |= (EXTI_INT2_SENSE_LEVEL_FALLING_EDGE << 6);
					Local_u8Error = STD_u8_OK;
				}
				else if(Copy_u8InterruptSenseLevel == EXTI_SENSE_LEVEL_RISING_EDGE)
				{
					CLR_BIT(EXTI_MCUCSR, 6);
					EXTI_MCUCSR |= (EXTI_INT2_SENSE_LEVEL_RISING_EDGE << 6);
					Local_u8Error = STD_u8_OK;
				}
				break;
		}
	}

	return Local_u8Error;

}



u8 EXTI_u8SetCallback(u8 Copy_u8InterruptNum, void (*Copy_Pu8CallbackFunction) (void))
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(  (Copy_u8InterruptNum == EXTI_u8_INT0) ||
		 (Copy_u8InterruptNum == EXTI_u8_INT1) ||
		 (Copy_u8InterruptNum == EXTI_u8_INT2)		)
	{
		Local_u8Error = STD_u8_OK;

		switch(Copy_u8InterruptNum)
		{
			case EXTI_u8_INT0:
				EXTI_PAVoidCallback[0] = Copy_Pu8CallbackFunction;
				break;
			case EXTI_u8_INT1:
				EXTI_PAVoidCallback[1] = Copy_Pu8CallbackFunction;
				break;
			case EXTI_u8_INT2:
				EXTI_PAVoidCallback[2] = Copy_Pu8CallbackFunction;
				break;
			default:
				Local_u8Error = STD_u8_ERROR;
				break;
		}


	}

	return Local_u8Error;
}
