/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          March 20th, 2019                    */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#ifndef EXTI_PRIVATE_H_
#define EXTI_PRIVATE_H_

#define EXTI_MCUCR			((Register*)0x55)->ByteAccess
#define EXTI_MCUCSR			((Register*)0x54)->ByteAccess
#define EXTI_GICR			((Register*)0x5B)->ByteAccess
#define EXTI_GIFR			((Register*)0x5A)->ByteAccess


#define EXTI_NO_OF_INTERRUPTS		(u8)3


#define EXTI_INT2_SENSE_LEVEL_FALLING_EDGE  	0b0
#define EXTI_INT2_SENSE_LEVEL_RISING_EDGE  		0b1

void __vector_1(void) __attribute__ ((signal));
void __vector_2(void) __attribute__ ((signal));
void __vector_3(void) __attribute__ ((signal));

#endif /* EXTI_PRIVATE_H_ */
